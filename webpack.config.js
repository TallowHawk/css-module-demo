const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const webpack = require('webpack')

const environment = process.env.NODE_ENV

const isProd = environment === 'production'

module.exports = {
    entry: './src/index.ts',
    mode: environment,
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'dist')
    },

    devtool: isProd ? false : 'cheap-source-map',

    devServer: {
        hot: true,
        inline: true,
        host: 'localhost',
        contentBase: path.join(__dirname, 'public'),
        port: 3010,
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        historyApiFallback: true,
        stats: {
            chunks: false,
            modules: false,
        },
    },

    module: {
        rules: [
            {
                test: /\.(ts|js)x?$/i,
                exclude: /node_modules/,
                resolve: {
                    extensions: ['.js', '.jsx', '.ts', '.tsx'],
                },
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/preset-env',
                            '@babel/preset-typescript',
                        ],
                        plugins: [
                            ['@babel/plugin-transform-react-jsx', {
                                pragma: 'h',
                                pragmaFrag: 'Fragment'
                            }],
                            '@babel/plugin-transform-runtime',
                            '@babel/plugin-proposal-class-properties'
                        ],
                    }
                }
            },
            {
                test: /\.(s[ac]ss|css)$/i,
                exclude: /node_modules/,
                use: [
                    // Creates `style` nodes from JS strings
                    'style-loader',
                    // Translates CSS into CommonJS
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true
                        }
                    },
                ],
            },
            {
                test: /\.(woff|woff2|eot|ttf)(\?.*$|$)/,
                use: 'url-loader'
            },
            {
                test: /\.ejs$/i,
                exclude: /node_modules/,
                use: 'ejs-loader',
            }
        ]
    },

    plugins: [
        new HtmlWebpackPlugin({
            title: 'Demo',
            template: 'src/index.ejs',
        }),
        new webpack.ProvidePlugin({
            h: ['preact', 'h'],
        })
    ],
}
